import React from 'react'
import logo from './logo.png'
import './App.css'

function App() {

    return (
        <div className="App">
            <main className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <h1>Marks Software</h1>
                <p>
                    Software consultancy and services
                </p>
                <p>
                    Since 2014
                </p>
                <h2>Founders</h2>
                <p>
                    <a
                        className="App-link"
                        href="https://www.linkedin.com/in/markalanrichards"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Mark Richards
                    </a>
                    {' | '}
                    <a
                        className="App-link"
                        href="https://www.linkedin.com/in/miefunahashi"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Mie Funahashi
                    </a>
                </p>
                <h2>Code experiments</h2>
                <p>
                    <a
                        className="App-link"
                        href="https://gitlab.com/markalanrichards"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        GitLab
                    </a>
                    {' | '}
                    <a
                        className="App-link"
                        href="https://github.com/markalanrichards"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        GitHub
                    </a>
                </p>
                <h2>Marks Software Limited</h2>
                <p>
                    Company number: 08681801
                </p>
                <p>
                    Registered office address: First Floor, Telecom House, 125-135 Preston Road, Brighton, England, BN1
                    6AF
                </p>
            </main>
        </div>
    )
}

export default App
